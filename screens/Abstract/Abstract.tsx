/**
 * Resumen de Area & Package Global
 * @author Brian Armando
 */
import * as React from 'react';
import { View, Platform, StyleSheet, SectionList, SafeAreaView, StatusBar, ActivityIndicator, Image, Alert } from 'react-native';
import { Screen, Scanner, Title } from '../../components/Kernel/IndexComponents';
import { MaterialIcons } from '@expo/vector-icons';
import { FloatingAction } from 'react-native-floating-action';
import { useThemeColor } from '../../components/Themes/Themed';
import { PacksGlobal, AreaGlobal } from '../../hooks/useControlAbstract'
import { fnDescompress } from '../../hooks/useControlHome'
import { CardCarousel } from './Carrusel';
import { Api } from '../../components/Connetions/index'
import numeral from 'numeral'
import Colors from '../../constants/Colors';

export default function Abstract({ route, navigation }) {

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Abstract',
      headerTitleStyle: { fontFamily: 'lato_black', fontSize: 22 }
    });
  }, [navigation]);

  const tam = Platform.OS == 'ios' ? 100 : 90
  const idPackage = route.params != undefined
    ? route.params.idPackage
    : Api.getSessionToken('npackage')

  const [scan, setScan] = React.useState<boolean>(false)
  const [didLoad, setDidLoad] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState<boolean>(false)
  const [section, setSection] = React.useState<any>([])
  const [id, setId] = React.useState<any>(null)

  React.useEffect(() => {
    if (idPackage && !didLoad) {
      fetchPackageGlobal(idPackage)
      setDidLoad(true)
    }
  })

  async function fetchPackageGlobal(idPackage: number) {
    setLoading(true)
    setId(idPackage)
    let aux: any = []
    let areas = await AreaGlobal(idPackage)
    let packs = await PacksGlobal(idPackage)

    let suma: number = 0
    for (let pack of areas) {
      suma = suma + parseFloat(pack.peso)
    }

    if (areas) {
      aux.push({
        title: 'Resumen Areas',
        data: areas.concat([{ key: 0, total: 'TOTAL', peso: suma }])
      })
    }

    if (packs) {
      aux.push({
        title: 'Resumen global x package',
        horizontal: true,
        data: packs,
      })
    }

    setLoading(false)
    setSection(aux)
  }

  const handleBarCodeScaner = async ({ type, data }) => {
    let idpack: any = fnDescompress(data)

    if (idpack) {
      setScan(false);
      setId(idpack)
      fetchPackageGlobal(idpack)
    } else {
      setScan(false);
      Alert.alert('No cumple con la estructura')
    }
  }

  const ListItem = ({ item }) => {
    return (
      <View key={item.key} style={styles.item}>
        <Title label={item.pieza ? item.pieza.ensamble.area.sCodigo : item.total} />
        <Title label={`${numeral(item.peso).format('0,0.00')} Kg`} />
      </View>
    );
  };

  return (
    <Screen>
      {loading && <ActivityIndicator size="large" color={Colors.light.s_dark} />}
      <StatusBar style="light" />
      {(idPackage || id) ? (
        <SafeAreaView style={{ flex: 1 }}>
          <SectionList
            contentContainerStyle={{ paddingHorizontal: 10, paddingBottom: 70 }}
            stickySectionHeadersEnabled={false}
            sections={section}
            renderSectionHeader={({ section }) => (
              <>
                <Title label={`${section.title}`} style={styles.sectionHeader} />
                {section.horizontal ? (
                  <CardCarousel carrusel={section.data} onPress={() => navigation.navigate('Details', { idPackage: id })} />
                ) : null}
              </>
            )}
            renderItem={({ item, section }) => {
              if (section.horizontal) {
                return null;
              }
              return <ListItem item={item} />;
            }}
          />
        </SafeAreaView>
      ) : (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginBottom: 30 }}>
          <Image
            style={styles.stretch}
            source={require('../../assets/images/fonts.png')}
          />
        </View>
      )
      }
      <FloatingAction
        color={useThemeColor({}, 'colorIcon')}
        onPressMain={() => setScan(true)}
        showBackground={false}
        distanceToEdge={{ vertical: tam, horizontal: 30 }}
        floatingIcon={
          <MaterialIcons
            name={'qr-code-scanner'}
            style={{ color: Colors.light.s_white }}
            size={25}
          />
        }
      />
      {scan && <Scanner visible={scan} scan={handleBarCodeScaner} onClose={() => setScan(false)} />}
    </Screen>
  );
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#121212',
  },
  sectionHeader: {
    fontWeight: '800',
    fontSize: 18,
    color: 'black',
    marginTop: 15,
    marginBottom: 5,
  },
  stretch: {
    width: 300,
    height: 300,
    resizeMode: 'stretch'
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.light.s_light,
    padding: 10,
    marginVertical: 8,
    borderRadius: 17
  },
  itemText: {
    color: 'rgba(255, 255, 255, 0.5)',
    marginTop: 5,
  },
});
