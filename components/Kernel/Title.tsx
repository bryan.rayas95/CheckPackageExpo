import React from "react";
import { Text, StyleSheet } from 'react-native'
import Layout from '../../constants/Layout'
export function Title(props: Text['props'] & any) {
   return (
      <Text style={{ ...style.title, ...props.style }}>{props.label}</Text>
   )
}

const style = StyleSheet.create({
   title: {
      fontSize: Layout.isSmallDevice ? 13 : 15,
      fontFamily: 'lato_black'
   }
})
