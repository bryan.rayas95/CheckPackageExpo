import React from "react";
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Subtitle } from "./Subtitle";
import { Title } from "./Title";
import { TextComun } from "./Text";
import { MaterialCommunityIcons, FontAwesome } from "@expo/vector-icons";
import { useThemeColor } from "../Themes/Themed";
export function CardList(props: any) {
    const iconColor = useThemeColor({}, 'secondary')
    return (
        <TouchableOpacity style={style.CardContainer} onPress={props.onPress}>
            <View style={style.Union}>
                <View style={[style.iCons, { backgroundColor: iconColor }]}>
                    <Subtitle style={[style.label, { color: useThemeColor({}, 's_white') }]} label={props.leyend} />
                    <FontAwesome name='truck' size={24} color={useThemeColor({}, 's_white')} />
                </View>
                <View style={{ justifyContent: 'flex-start', marginLeft: 15 }}>
                    <Title label={props.title} />
                    <Subtitle label={props.subtitle} />
                    <TextComun label={props.date} />
                </View>
            </View>
            <MaterialCommunityIcons name='clipboard-list-outline' size={24} color={useThemeColor({}, 's_dark')} />
        </TouchableOpacity >
    )
}

const style = StyleSheet.create({
    CardContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        marginRight: 7
    },
    Union: {
        flexDirection: "row",
        alignItems: 'center'
    },
    iCons: {
        width: 60,
        height: 60,
        borderRadius: 15,
        backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontFamily: 'regular-anton',
        fontSize: 15
    }
})