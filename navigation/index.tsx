/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import * as React from 'react';
import { ColorSchemeName, StyleSheet, Platform } from 'react-native';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { HomeStackScreen } from './Home/NavigationHome';
import { SettingsStackScreen } from './Settings/NavigationSettings'
import { RootTabParamList } from '../types';
import LinkingConfiguration from './LinkingConfiguration';
import Login from '../screens/Login/LoginScreen';
import HomeScreen   from '../screens/ListPackageHome/PackageListHome'
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <PrincipalScreen />
      {/*<BottomTabNavigator />*/}
    </NavigationContainer>
  );
}

const StackScreen = createNativeStackNavigator();
export function PrincipalScreen() {

  return (
    <StackScreen.Navigator
      screenOptions={({ route, navigation }) => ({
        headerShown: false,
      })} >

      <StackScreen.Screen
        name="Home"
        component={Login}
        
      />
      <StackScreen.Screen
        name="Tabs"
        component={BottomTabNavigator}
      />
    </StackScreen.Navigator>
  );
}
/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */

const Tab = createBottomTabNavigator<RootTabParamList>();

export function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <Tab.Navigator

      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: Colors[colorScheme].colorIcon,
        tabBarLabelPosition: 'below-icon',
        tabBarLabelStyle: {
          fontSize: 13,
          fontFamily: 'lato_black'
        },
        tabBarStyle: {
          position: 'absolute',
          bottom: 15,
          left: 20,
          right: 20,
          elevation: 0,
          backgroundColor: Colors.light.p_light,
          borderRadius: 15,
          height: Platform.OS == 'ios' ? 75 : 50,
          ...style.shadow
        }
      }}

    >
      <Tab.Screen
        name="List"
        component={HomeStackScreen}
        options={{
          title: 'List',
          tabBarIcon: ({ color }) => <TabBarIcon name="format-list-bulleted-triangle" color={color} />,
        }}
      />
      <Tab.Screen
        name="Abstracts"
        component={SettingsStackScreen}
        options={{
          title: 'Abstract',
          tabBarIcon: ({ color }) => <TabBarIcon name="package-variant" color={color} />,
        }}
      />
    </Tab.Navigator>
  )
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
  name: React.ComponentProps<typeof MaterialCommunityIcons>['name'];
  color: string;
}) {
  return <MaterialCommunityIcons size={35} style={{ marginBottom: -3 }} {...props} />;
}
const style = StyleSheet.create({
  shadow: {
    shadowColor: '#b4e0ce',
    shadowOffset: {
      width: 0,
      height: 10
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5
  }
})