/**
 * Prepare app instance
 *
 */
const fs = require('fs')
const path = require('path')

/**
 * setApp
 *
 * @param {{ name: string, description: string, slug: string }} param0
 */
function setApp({ name, description, slug }) {
  const appFileName = path.resolve('./', 'app.json')
  const obj = JSON.parse(fs.readFileSync(appFileName))

  obj.expo.name = name
  obj.expo.description = description
  obj.expo.slug = slug

  fs.writeFileSync(appFileName, JSON.stringify(obj, null, 4))
}

/**
 * setAppEnv
 *
 */
function setAppEnv() {
  setApp({
    name: process.env.APP_NAME,
    description: process.env.APP_DESCRIPTION,
    slug: process.env.APP_SLUG,
  })
}

module.exports.setApp = setApp
module.exports.setAppEnv = setAppEnv