import React from "react";
import { View } from 'react-native'
import { ThemeProps, useThemeColor } from '../Themes/Themed'

export function Screen(props: ThemeProps & View['props']) {
    const { style, ...otherProps } = props;
    const backgroundColor = useThemeColor({}, 'primary');
    return (
        <View {...props} style={[{ flex: 1, position: 'relative', padding: 8 }, { backgroundColor }, style]}>
            {props.children}
        </View>
    )
}