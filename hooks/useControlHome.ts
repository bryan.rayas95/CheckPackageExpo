import { Api, Exception } from '../components/Connetions/index'
/**
 * Retrive All Package List
 * @returns []
 */
export async function RetriveAllPackage(page = 0, search = '') {
    try {
        let response = null

        response = await Api.query({
            model: 'ConsPackage',
            attributes: ['iId', 'sFolio', 'sTraspaso', 'dKernelFechaRegistro'],
            include: [
                {
                    as: 'registra',
                    attributes: ['sNombre']
                }
            ],
            where: {
                iActivo: true,
                iIdProyecto: Api.getSessionToken('contract'),
                ...(search.length > 0 ? {
                    $or: [
                        { '$sFolio$': { $like: `%${search}%` } }
                    ]
                } : null)
            },
            order: [['iId', 'Desc']],
            limit: Api.pageSize,
            offset: Api.pageSize * page
        })

        if (response) return response.data

        return null
    } catch (error) {
        Exception.load(error).show()
    }
}

/**
 * Retrive one package
 * @param id number
 * @returns 
 */
export async function RetriveOnePackage(id: number = 0) {
    try {
        let response = null
        response = await Api.query({
            __returnType: 'one',
            model: 'ConsPackage',
            attributes: [
                'iId',
                'iIdProceso',
                'sFolio',
                'sPackingStyle',
                'sMainDescription',
                'sTraspaso',
                'dKernelFechaRegistro',
                'iIdProyecto',
                'sExcedentes'
            ],
            include: [
                {
                    as: 'registra',
                    attributes: ['sNombre']
                },
                {
                    as: 'proyecto',
                    attributes: ['sAlias']
                }
            ],
            where: {
                iActivo: true,
                iId: id
            }
        })

        if (response) return response.data

        return null
    } catch (error) {
        Exception.load(error).show()
    }

}

/**
 * Retrive All items package
 */
export async function RetriveAllItemsPackage(idPackage: number, search: string = '') {
    try {
        let response: any = []

        response = await Api.query({
            model: 'ConsPackageList',
            attributes: [
                ['iId', 'key'],
                'iIdPieza',
                ['sPacking', 'name'],
                'iRecibida',
                [{ key: 'fn', name: 'concat', params: ['$pieza->ensamble.sCodigo$', '  ', '$pieza.iNumero$', '/', '$pieza->ensamble.dCantidadTotal$'] }, 'title'],
            ],
            include: [
                {
                    as: 'pieza',
                    requiered: true,
                    attributes: [],
                    include: [
                        {
                            as: 'ensamble',
                            required: true,
                            attributes: []
                        }
                    ]
                }
            ],
            where: {
                iActivo: true,
                iIdPackage: idPackage,
                ...(search.length > 0 ? {
                    $or: [
                        { '$pieza->ensamble.sCodigo$': { $like: `%${search}%` } },
                        { '$sPacking$': { $like: `%${search}%` } }
                    ]
                } : null)
            }
        })

        if (response) return response.data

        return []

    } catch (error) {
        Exception.load(error).show()
    }

}

/**
 *  Just retrieve the names of the packages inside this package
 * @param idPackage 
 * @returns 
 */
export async function RetriveOnlyNamePackage(idPackage: number) {
    try {
        let response: any = null

        response = await Api.query({
            model: 'ConsPackage',
            __returnType: 'one',
            attributes: [
                'iId',
                'iIdProceso',
                'sFolio',
                'sPackingStyle',
                'sMainStructure'
            ],
            include: [
                {
                    as: 'registra',
                    attributes: ['sNombre']
                },
                {
                    as: 'proyecto',
                    attributes: ['sAlias']
                }
            ],
            where: {
                iActivo: true,
                iId: idPackage
            }
        })

        if (response) return response.data

        return []

    } catch (error) {
        Exception.load(error).show()
    }
}

/**
 * Confirm exist piece in list of package
 * @param idPackage
 * @param idPieza
 */
export async function ConfirmPiecePackage(idPieza: number, idPackage: number) {
    try {
        let response = null

        response = await Api.put(`/cons/package-list/confirm/pack=${idPackage}&&pieza=${idPieza}`)
        if (response) return response.data

        return null
    } catch (error) {
        Exception.load(error).show()
    }
}

/**
 * Extraer IdPieza
 * @param data 
 * @returns 
 */
export function fnIngBuscarPiezaQR(data: string) {

    const regex = /([A-Z0-9a-z\'\.\s\(\)\-]*)(\:)([A-Z0-9a-z\'\.\s\(\)\-]*)(\:)([A-Z0-9a-z\'\.\s\(\)\-]*)(\/)([0-9]*)(-)([0-9]*)/g
    const dataOk = regex.test(data)

    if (!dataOk) {
        return null
    }

    const [proyecto, ensamble, pieza] = data.split(':')
    const numero = pieza.split('/')[0]
    const [_, id] = pieza.split('-')

    return id
}


/**
 * Extraer idPackage
 * @param data
 */

export function fnDescompress(data: string) {

    const regex = /([A-Z0-9a-z\'\.\s\(\)\-]*)(\:)([A-Z0-9a-z\'\.\s\/\-]*)(\:)([0-9]*)/g
    const packageOk = regex.test(data)

    if (!packageOk) {
        return null
    }

    const [proyecto, folio, id] = data.split(':')

    return id
}

/**
 * Retrive Contrato validate Login
 */

export async function RetriveOneContract(code: string) {
    try {
        let response = null
        response = await Api.query({
            __returnType: 'one',
            model: 'CmrProject',
            attributes: ['iId', 'sAlias', 'sCodigo'],
            where: {
                iActivo: true,
                sCodigo: code
            }
        })

        if (response) return response.data

        return null
    } catch (error) {
        Exception.load(error).show()
    }
}

/**
 * Retrive all placas sueltas
 */
export async function PlacasSueltasInPackage(idPackage: number, search = '') {
    try {
        let tpPlacas = null

        tpPlacas = await Api.query({
            model: "ConsPackageList",
            attributes: [
                [{ fn: 'count', params: ["$pieza.iId$"] }, 'numPiezas'],
                [{ fn: 'concat', params: ['$pieza->ensamble.sCodigo$'] }, 'mark'],
                [{ fn: 'fn_cons_traspaso_elemento_suelto_id', params: [-1, '$pieza->ensamble.iIdArea$', '$pieza->ensamble.sCodigo$', '$pieza->partes2.sSubMarca$', idPackage] }, 'idElement'],
                [{ fn: 'fn_cons_traspaso_elementos_sueltos', params: [-1, '$pieza->ensamble.iIdArea$', '$pieza->ensamble.sCodigo$', '$pieza->partes2.sSubMarca$', idPackage] }, 'cant']
            ],
            include: [
                {
                    as: "pieza",
                    attributes: ["iId"],
                    required: true,
                    include: [
                        {
                            as: "ensamble",
                            required: true,
                            attributes: ['iId', 'iIdArea'],
                            where: {
                                iActivo: true,
                                ...(search.length > 0 ? { sCodigo: { $like: `%${search}%` } } : null)
                            }
                        },

                        {
                            as: 'partes2',
                            attributes: ['iId', 'sSubMarca', 'dCantidad'],
                            required: true,
                            where: {
                                iActivo: true,
                                sSubMarca: {
                                    "$or": [
                                        { "$like": "LSP%" },
                                        { "$like": "LP%" },
                                        { "$like": "LCL%" },
                                        { "$like": "LFP%" }
                                    ]
                                }
                            }
                        }
                    ]
                }
            ],
            where: {
                iActivo: true,
                iIdPackage: idPackage
            },
            group: ['iIdArea', 'sCodigo', 'sSubMarca']
        })

        if (tpPlacas) {
            let tmpPlacas = tpPlacas.data.map(item => ({
                ...item,
                Tpzas: parseInt(item.pieza.partes2.dCantidad) * parseInt(item.numPiezas),
                cont: item.cant
            }))

            return tmpPlacas
        }

        return []
    } catch (err) {
        Exception.load(err).show()
    }
}

/**
 * Retrive all placas sueltas
 */
export async function AllPlacasSueltasInPackage(idPackage: number,) {
    try {
        let allPlacas = null

        allPlacas = await Api.query({
            model: "ConsEstructuraEnvioParte",
            __returnType: 'one',
            attributes: [
                [{ fn: 'sum', params: ["$dCantidad$"] }, 'total'],
            ],
            where: {
                iActivo: true,
                iIdPacking: idPackage
            }
        })

        if (allPlacas) return allPlacas.data

        return null
    } catch (err) {
        Exception.load(err).show()
    }
}