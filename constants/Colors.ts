const tintColorLight = '#5FC2E6';
const tintColorDark = '#fff';

export default {
  light: {
    primary: '#E0E0E0',
    p_light: '#ffffff',
    p_dark: '#aeaeae',
    p_black: '#000000',
    secondary: '#1565c0',
    s_light: '#5e92f3',
    s_dark: '#003c8f',
    s_white: '#ffffff',

    headerColor: '#DDDCDE', // GRAY 
    varHeader: '#E9E9E8',   // GRAY/WHITE
    withe: '#FEC92D',
    background: '#E1E1E1',  // WHITE
    tint: tintColorLight,
    colorIcon: '#1565c0', // PURPLE/BLUE
    twopurple: '#2F2C74', // VARIACION MORADO
    purple: '#2E2E71',    //PURPLE
    backColor: '#FDFDFD', // backColor (mediun white)
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    firstFondo: '#E1E1E1',
    text: '#000',
  },

  dark: {
    primary: '#E0E0E0',
    p_light: '#ffffff',
    p_dark: '#aeaeae',
    p_black: '#000000',
    secondary: '#1565c0',
    s_light: '#1565c0',
    s_dark: '#003c8f',
    s_white: '#ffffff',

    headerColor: '#DDDCDE', // GRAY 
    varHeader: '#E9E9E8',   // GRAY/WHITE
    background: '#E1E1E1',  // white
    withe: '#E9E9E8',
    colorIcon: '#2E2E71', // PURPLE/BLUE
    twopurple: '#2F2C74', // VARIACION MORADO
    purple: '#2E2E71',    //PURPLE
    backColor: '#FDFDFD', // backColor (mediun white)
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    firstFondo: '#E1E1E1',
    text: '#f3ED',
  },

};
