import React from "react";
import { View } from 'react-native'
import { Title } from "./Title";
import { Subtitle } from "./Subtitle";


export function Label(props: any) {
    return (
        <View style={props.styleView}>
            <Subtitle label={props.lbl} style={{ paddingBottom: 0, ...props.sLbl }} />
            <Title label={props.Tlbl} style={{ padding: 0, margin: 0, ...props.tLbl }} />
        </View>
    )
}