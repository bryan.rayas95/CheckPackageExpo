import { FontAwesome } from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import * as React from 'react';

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...FontAwesome.font,
          'space-mono': require('../assets/fonts/SpaceMono-Regular.ttf'),
          'andalas': require('../assets/fonts/ANDALAS.ttf'),
          'baxoe': require('../assets/fonts/Baxoe.ttf'),
          'regular-anton': require('../assets/fonts/Anton-Regular.ttf'),
          'bebas-regular': require('../assets/fonts/BebasNeue-Regular.ttf'),
          'crimson': require('../assets/fonts/CrimsonText-Bold.ttf'),
          'lodrina' : require('../assets/fonts/LondrinaSolid-Regular.ttf'),
          'dirtytown': require('../assets/fonts/dirtyoldtown.ttf'),
          'sophia': require('../assets/fonts/emmasophia.ttf'),
          'lato_black': require('../assets/fonts/Lato-Black.ttf'),
          'lato_bold': require('../assets/fonts/Lato-Bold.ttf'),
          'roboto_light': require('../assets/fonts/Roboto-Light.ttf'),
          'roboto_medium': require('../assets/fonts/Roboto-Medium.ttf'),
          'roboto_thin': require('../assets/fonts/Roboto-Thin.ttf'),
        });

      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
