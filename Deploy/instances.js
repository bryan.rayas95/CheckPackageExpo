/**
 * Instances info
 *
 */
module.exports = [

  {
    name: 'Check-Package-List',
    description: 'Verificador de piezas enviadas en los embarques para proyectos en sitio',
    slug: 'packageMepi',
    connections: [
      {
        title: 'MEPI',
        port: 4018,
        url: 'http://187.217.102.58:4018'
      }
    ],
  },

  {
    name: 'TESTING',
    description: 'Testing',
    slug: 'testing',
    connections: [
      {
        title: 'TESTING',
        port: 4002,
        url: 'http://187.217.102.58:4002'
      },
      {
        title: 'DEVELOPERS',
        port: 4003,
        url: 'http://187.217.102.58:4003'
      },
    ],
  },
]