import React from 'react';
import { View, StyleSheet, FlatList, Modal, Platform } from 'react-native'
import Colors from '../../constants/Colors';
import { RetriveOnePackage } from '../../hooks/useControlHome'
import { Ionicons } from '@expo/vector-icons'
import { Separator } from './Separator';
import { Title } from './Title';
import { Subtitle } from './Subtitle';

interface ListProps {
    visible: boolean
    onClose?(): void
    idPackage: number
}

export function ModalList({ visible, onClose, idPackage }: ListProps): any {

    const [data, setData] = React.useState([])

    React.useEffect(() => {
        if (visible && idPackage) {
            fetchOneReport(idPackage)
        }
    }, [visible])

    async function fetchOneReport(idPackage: number) {
        let pack = await RetriveOnePackage(idPackage)

        if (pack.sExcedentes) {
            setData(pack.sExcedentes.split(','))
        }
    }

    return (
        <View style={[style.modalContainer,]}>
            <Modal
                transparent={true}
                animationType='fade'
                visible={visible}
                onRequestClose={onClose}>
                <View style={[style.modalContainer, { marginVertical: Platform.OS == 'ios' ? -130 : -100 }]}>
                    <View style={style.container}>
                        <View style={style.BarCodeBox}>
                            <View style={style.title}>
                                <Title label={"Excedentes"} />
                            </View>
                            <Separator />
                            <FlatList
                                contentContainerStyle={{ paddingBottom: 20 }}
                                data={data}
                                keyExtractor={(_, idx) => idx.toString()}
                                ItemSeparatorComponent={Separator}
                                renderItem={({ item, index }) => (
                                    <View>
                                        <Subtitle label={item} />
                                    </View>
                                )}

                            />
                        </View>
                        <Ionicons
                            name={'close-circle'}
                            onPress={onClose}
                            size={35}
                            style={{ color: Colors.light.p_dark }}
                        />
                    </View>
                </View>
            </Modal>
        </View>
    )
}


const style = StyleSheet.create({
    modalContainer: {
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        position: 'absolute',
        marginLeft: 30,
        height: '100%',
        width: '100%',

    },
    container: {
        alignItems: 'center',
        borderRadius: 30,
    },
    BarCodeBox: {
        backgroundColor: Colors.light.p_light,//'#000000aa',
        height: 365,
        width: 300,
        overflow: 'hidden',
        borderRadius: 10,
        padding: 5
    },
    boton: {
        borderRadius: 16,
        width: 100,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 8
    }
})