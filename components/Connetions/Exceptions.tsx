import Api from './Api'
import { Alert } from 'react-native'

/**
 * Exceptions
 * constainer Errors
 */
class Exception {
    message: string;

    /**
     * Constructor
     */

    constructor(e: any, message: string = 'Se encontraron problemas') {
        this.message = (e.response ? Api.statusCodeToString(e.response.status, e.response.data) : message)

    }

    static load(error: any, msg = undefined) {
        return new Exception(error, msg);
    }
    show() {
        Alert.alert(this.message)
    }
}

export default Exception;