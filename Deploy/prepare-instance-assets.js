/**
 * Prepare instance assets
 *
 */
const fs = require('fs')
const path = require('path')

/**
 * copyAssets
 *
 * @param {string} instance_name
 */
function copyAssets(instance_name) {
  const dest = path.join(process.cwd(), 'assets')
  const instancePath = path.join(process.cwd(), 'assets/images', instance_name)

  const files = [
    'logo.png',
  ]

  for (const file of files) {
    const filePath = path.join(instancePath, file)
    fs.copyFileSync(filePath, path.join(dest, file))
  }

}

/**
 * copyAssetsEnv
 *
 */
function copyAssetsEnv() {
  copyAssets(process.env.APP_ASSETS.toLocaleLowerCase())
}

module.exports.copyAssets = copyAssets
module.exports.copyAssetsEnv = copyAssetsEnv