import * as React from 'react';
import { StyleSheet, Platform, RefreshControl, FlatList, Alert } from 'react-native';
import { View } from 'react-native'
import { Screen, Scanner, SearchBar, IconsHeaderRight, CardList } from '../../components/Kernel/IndexComponents';
import { Api } from '../../components/Connetions/index'
import { RetriveAllPackage, RetriveOnePackage, fnDescompress } from '../../hooks/useControlHome'
import { FloatingAction } from 'react-native-floating-action';
import { MaterialIcons } from '@expo/vector-icons';
import moment from 'moment';
import { useThemeColor } from '../../components/Themes/Themed';

export default function HomeScreen({ route, navigation }) {
  const [loading, setLoading] = React.useState(false)
  const [visible, setVisible] = React.useState(false)
  const [packages, setPackages] = React.useState<any>([])
  const [didLoad, setDidLoad] = React.useState<boolean>(false);
  const [scan, setScan] = React.useState<boolean>(false)
  const [page, setPage] = React.useState<number>(0)
  const [canLoad, setCan] = React.useState<boolean>(true)

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: visible ? '' : 'Package List',
      headerTitleAlign: 'center',
      headerTitleStyle: { fontFamily: 'lato_black', fontSize: 22 },
      headerRight: () => (
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: Platform.OS == 'ios' ? 'center' : 'flex-end', }}>
          {visible && <SearchBar onSearch={(text: any) => fetch(0, text)} />}
          <IconsHeaderRight
            name1={visible ? 'close' : "search"}
            name2={"rotate-right"}
            size1={visible ? 25 : 20}
            press1={() => visible ? actions() : setVisible(true)}
            press2={() => fetch()} />
        </View>
      ),
    });
  }, [navigation, visible]);

  const fetch = async (page: number = 0, search: string = '') => {
    setLoading(true)
    setPage(page)
    let response = await RetriveAllPackage(page, search)

    if (response) {
      setPackages(
        page > 0
          ? packages.concat(response)
          : response
      )
      setCan(response.length >= Api.pageSize)
      setLoading(false)
    }
  }

  const actions = () => {
    if (visible) {
      fetch()
      setVisible(false)
    } else {
      setVisible(true)
    }
  }

  React.useLayoutEffect(() => {
    if (!didLoad) {
      fetch()
      setDidLoad(true)
    }
  }, [didLoad])

  const handleBarCodeScaner = async ({ type, data }) => {
    let idpack: any = fnDescompress(data)

    if (idpack) {
      let oneReport = await RetriveOnePackage(idpack)
      if (oneReport) {
        setScan(false);
        navigation.navigate('Details', { idPackage: oneReport.iId })
      } else {
        setScan(false);
        Alert.alert('No cumple con la estructura')
      }
    }
  }

  const tam = Platform.OS == 'ios' ? 100 : 70
  return (
    <Screen>
      <FlatList
        data={packages}
        keyExtractor={(_, idx) => idx.toString()}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={() => fetch()}
          />
        }
        contentContainerStyle={{ paddingBottom: 75 }}
        renderItem={({ item }) =>
          <CardList
            onPress={() => navigation.navigate('Details', { idPackage: item.iId })}
            title={item.sFolio}
            subtitle={item.registra.sNombre}
            leyend={item.sTraspaso}
            date={moment(item.dKernelFechaRegistro).format('YYYY-MM-DD HH:MM')}
          />
        }
        onEndReached={() => !loading && canLoad && fetch(page + 1)}
      />
      <FloatingAction
        color={useThemeColor({}, 'colorIcon')}
        onPressMain={() => setScan(true)}
        showBackground={false}
        distanceToEdge={{ vertical: tam, horizontal: 30 }}
        floatingIcon={
          <MaterialIcons
            name={'qr-code-scanner'}
            style={{ color: 'white' }}
            size={25}
          />
        }
      />
      {scan && <Scanner visible={scan} scan={handleBarCodeScaner} onClose={() => setScan(false)} />}
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
