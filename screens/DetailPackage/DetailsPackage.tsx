import * as React from 'react';
import { StyleSheet, FlatList, View, Platform, Dimensions, RefreshControl, TouchableOpacity, Alert, Text } from 'react-native';
import { Screen, Title, CardTrafficLights, Scanner, Subtitle, ModalList, ModalPlacasSueltas } from '../../components/Kernel/IndexComponents'
import { RetriveAllItemsPackage, RetriveOnePackage, ConfirmPiecePackage, fnIngBuscarPiezaQR } from '../../hooks/useControlHome';
import { FloatingAction } from 'react-native-floating-action';
import { MaterialIcons } from '@expo/vector-icons';
import Layout from '../../constants/Layout';
import { Api } from '../../components/Connetions';
import { useThemeColor } from '../../components/Themes/Themed';
import Colors from '../../constants/Colors';

const { width } = Dimensions.get('window');
const val = Layout.isSmallDevice ? (width - 16) / 4 : (width - 16) / 4

export default function Details({ route, navigation }) {

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Details',
            headerTitleStyle: { fontFamily: 'lato_black', fontSize: 22 }
        });
    }, [navigation]);

    const flatListRef = React.useRef<FlatList>()
    const { idPackage } = route.params
    const [loading, setLoading] = React.useState<boolean>(false)
    const [itemsPac, setItems] = React.useState<any>([])
    const [scan, setScan] = React.useState<boolean>(false)
    const [namePac, setName] = React.useState<any>({})
    const [list, setList] = React.useState<boolean>(false)
    const [didLoad, setDidLoad] = React.useState<boolean>(false);
    const [idx, setIndex] = React.useState<number>(-1)
    const [placas, setPlacas] = React.useState<boolean>(false)
    const tam = Platform.OS == 'ios' ? 100 : 70

    const actions = [
        {
            text: "Excedentes",
            icon: require("../../assets/images/tag.png"),
            name: "excedentes",
            position: 2
        },
        {
            text: "Placas sueltas",
            icon: require("../../assets/images/steel.png"),
            name: "placas",
            position: 1
        }]

    React.useEffect(() => {
        if (idPackage && !didLoad) {
            setDidLoad(true)
            Api.setSessionToken(idPackage, 'npackage')
            fetchNamePackage(idPackage)
            fetchAllItemsPackage(idPackage)
        }
    })

    async function fetchAllItemsPackage(idPackage: number) {
        setLoading(true)
        let response = await RetriveAllItemsPackage(idPackage)
        if (response) {
            setItems(response)
            setLoading(false)
        }
    }

    async function fetchNamePackage(idPackage: number) {
        setLoading(true)
        let name = await RetriveOnePackage(idPackage)
        if (name) {
            setName(name)
            setLoading(false)
        }
    }

    async function checkPiece({ type, data }) {
        let Pieza: any = fnIngBuscarPiezaQR(data)
        if (Pieza) {
            setScan(false)
            let check = await ConfirmPiecePackage(Pieza, idPackage)

            if (check) {
                let index = itemsPac.findIndex((item: any) => item.key == check.iId)
                let aux = [...itemsPac]
                aux[index].iRecibida = check.iRecibida
                setItems(aux)

                scrollToIndex(index)
            }

        } else {
            setScan(false)
            Alert.alert('No cumple con la estructura')
        }
    }

    const getItemLayout = (data: any, index: number) => (
        { length: 50, offset: 50 * index, index }
    )

    const scrollToIndex = (index: number) => {
        setIndex(index)
        flatListRef.current?.scrollToIndex({ animated: true, index: index });
    }

    const StyleText = (index: number) => {
        if (index == idx && !Layout.isSmallDevice) return styles.oneStyle
        else if (index == idx && Layout.isSmallDevice) return styles.secondStyle
        return {}
    }

    return (
        <Screen>
            <TouchableOpacity style={[styles.HeaderTable]} onPress={() => navigation.navigate('Abstract', { idPackage: idPackage })}>
                <View style={styles.header} />
                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                    <Title label={namePac.sFolio} />
                    <Subtitle label={namePac.iIdProyecto && namePac.proyecto.sAlias} />
                </View>
            </TouchableOpacity>
            <FlatList
                style={{ flex: 1 }}
                ref={flatListRef}
                refreshControl={
                    <RefreshControl
                        refreshing={loading}
                        onRefresh={() => fetchAllItemsPackage(idPackage)}
                    />
                }
                contentContainerStyle={{ paddingBottom: 100, paddingTop: 10 }}
                data={itemsPac}
                keyExtractor={(_, idx) => idx.toString()}
                getItemLayout={getItemLayout}
                initialScrollIndex={0}
                initialNumToRender={2}
                renderItem={({ item, index }) => (
                    <CardTrafficLights key={index} title={StyleText(index)} description={item.description} lblName={item.title} pack={item.name} value={item.iRecibida} />
                )}

            />

            <FloatingAction
                color={useThemeColor({}, 'colorIcon')}
                onPressMain={() => setScan(true)}
                showBackground={false}
                distanceToEdge={{ vertical: tam, horizontal: 30 }}
                floatingIcon={
                    <MaterialIcons
                        name={'qr-code-scanner'}
                        size={25}
                        style={{ color: Colors.light.s_white }}
                    />
                }
            />
            <FloatingAction
                color={useThemeColor({}, 'colorIcon')}
                actions={actions}
                showBackground={false}
                distanceToEdge={{ vertical: tam, horizontal: 30 }}
                position={'left'}
                floatingIcon={
                    <MaterialIcons
                        name={'filter-list'}
                        size={25}
                        style={{ color: Colors.light.s_white }}
                    />
                }
                onPressItem={name => {
                    switch (name) {
                        case 'placas':
                            setPlacas(true)
                            break;
                        case 'excedentes':
                            setList(true)
                            break;
                        default:
                            alert('No existe esa opción')
                    }
                }
                }
            />
            {scan && <Scanner visible={scan} onClose={() => setScan(false)} scan={checkPiece} />}
            {list && <ModalList visible={list} onClose={() => setList(false)} idPackage={idPackage} />}
            {placas && <ModalPlacasSueltas visible={placas} onClose={() => setPlacas(false)} idPackage={idPackage} />}
        </Screen>
    );
}

const styles = StyleSheet.create({
    HeaderTable: {
        height: 40,
        backgroundColor: Colors.light.s_light,
        width: (val * 4) - 15,
        marginHorizontal: 8,
        marginVertical: 20,
        borderRadius: 37,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        elevation: 1
    },
    header: {
        backgroundColor: Colors.light.s_light,
        position: 'relative',
        bottom: 5,
        borderRadius: 20,
        height: 20,
        width: val * 3,
        opacity: 3
    },
    label: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        flex: 1
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    oneStyle: {
        fontSize: 17,
        color: Colors.light.s_dark
    },
    secondStyle: {
        fontSize: 15,
        color: Colors.light.s_dark
    }
});
