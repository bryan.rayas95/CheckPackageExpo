import React from "react";
import { View, StyleSheet } from "react-native";
import { ThemeProps, useThemeColor } from '../Themes/Themed'

export function Separator(props: ThemeProps & View['props']) {
    const { style, lightColor, darkColor, ...otherProps } = props;
    //lightColor="#eee" darkColor="rgba(255,255,255,0.9)
    const backgroundColor = useThemeColor({ light: '#90AFDC', dark: '#E1E1E1aa'}, 'background');
    return (
        <View style={[styles.separator, { backgroundColor }, style]} />
    )
}

const styles = StyleSheet.create({
    separator: {
        marginVertical: 10,
        height: 1,
        width: '100%',
    },
})