import React from 'react'
import { View } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import { useThemeColor } from '../Themes/Themed'

export function IconsHeaderRight(props: any) {
    return (
        <View style={{ flexDirection: 'row' }}>
            <FontAwesome
                name={props.name1}
                size={props.size1}
                color={useThemeColor({},'s_dark')}
                style={{ marginLeft: 10, marginRight: 20 }}
                onPress={props.press1}
            />
            <FontAwesome
                name={props.name2}
                color={useThemeColor({},'s_dark')}
                size={25}
                onPress={props.press2}
            />
        </View>
    )
}