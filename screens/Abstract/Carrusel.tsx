import React from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import { Label, Text } from '../../components/Kernel/IndexComponents'
import Carousel from 'react-native-snap-carousel';
import { scrollInterpolator, animatedStyles } from './Utils/Animations';
import numeral from 'numeral';
import Colors from "../../constants/Colors";

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

export function CardCarousel(props: any) {

    const refCar = React.useRef()
    const [activeIndex, setIndex] = React.useState(0)

    function _renderItem({ item, index }) {
        var div = item.pieza.ensamble.area.sCodigo.split(" ", 2);

        return (
            <View style={styles.itemContainer}>
                <Label lbl={'Package No'} Tlbl={item.name} />
                <TouchableOpacity style={styles.boton} onPress={props.onPress}>
                    <Label styleView={styles.qyt} lbl={div} sLbl={{ fontSize: 15 }} tLbl={styles.itemLabel} Tlbl={numeral(item.cant).format('0')} />
                    <View style={{ marginLeft: 10, justifyContent: 'space-around' }}>
                        <Label styleView={{ marginBottom: 5 }} lbl={'Package Style'} Tlbl={item.package.sPackingStyle} />
                        <Label styleView={{ marginBottom: 5 }} lbl={'Main Item Description'} Tlbl={item.package.sMainDescription} />
                        <Label styleView={styles.reverse} lbl={'N.W.(KGS)'} Tlbl={`${numeral(item.peso).format('0,0.00')}  `} />
                        <Label styleView={styles.reverse} lbl={'G.W.(KGS)'} Tlbl={`${numeral(item.peso).format('0,0.00')}  `} />
                    </View>
                </TouchableOpacity>
                <View style={styles.gralView}>
                    <Label lbl={'Lenght (cm)'} Tlbl={numeral(item.dDimLength).format('0,0.00')} />
                    <Label lbl={'Wide (cm)'} Tlbl={numeral(item.dDimWide).format('0,0.00')} />
                    <Label lbl={'Height (cm)'} Tlbl={numeral(item.dDimHeight).format('0,0.00')} />
                    <Label lbl={'G.V.(CBM)'} Tlbl={numeral(item.dGV).format('0,0.00')} />
                </View>
            </View>
        );
    }

    return (
        <View>
            <Carousel
                ref={refCar}
                data={props.carrusel}
                renderItem={_renderItem}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH + 50}
                containerCustomStyle={styles.carouselContainer}
                inactiveSlideShift={0}
                onSnapToItem={index => setIndex(index)}
                scrollInterpolator={scrollInterpolator}
                slideInterpolatedStyle={animatedStyles}
                useScrollView={true}
            />
            {CardCarousel.length > 0 && <Text style={styles.counter} label={activeIndex + 1} />}
        </View>
    )
}

const styles = StyleSheet.create({
    carouselContainer: {
        marginTop: 20,
    },
    itemContainer: {
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        justifyContent: 'space-around',
        padding: 20,
        backgroundColor: Colors.light.s_light,
        borderRadius: 17
    },
    qyt: {
        width: ITEM_WIDTH / 4,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.light.primary,
        borderRadius: 150
    },
    gralView: {
        flexDirection: "row",
        justifyContent: 'space-around'
    },
    itemLabel: {
        fontSize: 40
    },
    counter: {
        marginTop: 1,
        fontSize: 20,
        textAlign: 'center'
    },
    reverse: {
        marginBottom: 5,
        flexDirection: 'row-reverse',
        justifyContent: 'flex-end'
    },
    boton: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10
    }
});