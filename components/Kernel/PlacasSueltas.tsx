import React from 'react';
import { View, StyleSheet, FlatList, Modal, Platform, Text } from 'react-native'
import Colors from '../../constants/Colors';
import { PlacasSueltasInPackage, AllPlacasSueltasInPackage } from '../../hooks/useControlHome'
import { Ionicons } from '@expo/vector-icons'
import { Separator } from './Separator';
import { Title } from './Title';
import { Subtitle } from './Subtitle';

interface ListProps {
    visible: boolean
    onClose?(): void
    idPackage: number
}

export function ModalPlacasSueltas({ visible, onClose, idPackage }: ListProps): any {

    const [data, setData] = React.useState([])
    const [total, setTotal] = React.useState(0)
    React.useEffect(() => {
        if (visible && idPackage) {
            fetchPlacasSueltas(idPackage)
        }
    }, [visible])

    async function fetchPlacasSueltas(idPackage: number) {
        let placas = await PlacasSueltasInPackage(idPackage)
        let num = await AllPlacasSueltasInPackage(idPackage)

        if (placas) {
            setData(placas)
            setTotal(num.total != null ? num : 0)
        }
    }
   
    const LabelNum = ({ text }) => (
        <View style={[style.title, { width: 40 }]}>
            <Subtitle label={text} />
        </View>
    )
    return (
        <View style={[style.modalContainer,]}>
            <Modal
                transparent={true}
                animationType='fade'
                visible={visible}
                onRequestClose={onClose}>
                <View style={[style.modalContainer, { marginVertical: Platform.OS == 'ios' ? -130 : -100 }]}>
                    <View style={style.container}>
                        <View style={style.BarCodeBox}>
                            <View style={style.item}>
                                <LabelNum text='REQ' />
                                <View style={style.title}>
                                    <Title label={`PLACAS SUELTAS:  ${total}`} />
                                </View>
                                <LabelNum text='ENV' />
                            </View>
                            <Separator />
                            <FlatList
                                contentContainerStyle={{ paddingBottom: 20 }}
                                data={data}
                                keyExtractor={(_, idx) => idx.toString()}
                                ItemSeparatorComponent={Separator}
                                renderItem={({ item, index }) => (
                                    <View style={style.item}>
                                        <LabelNum text={item.Tpzas} />

                                        <View style={style.title}>
                                            <Title label={`${item.mark}`} />
                                            <Subtitle label={`${item.pieza.partes2.sSubMarca}`} />
                                        </View>
                                        <LabelNum text={item.cant} />
                                    </View>
                                )}

                            />
                        </View>
                        <Ionicons
                            name={'close-circle'}
                            onPress={onClose}
                            size={35}
                            style={{ color: Colors.light.p_dark }}
                        />
                    </View>
                </View>
            </Modal>
        </View>
    )
}


const style = StyleSheet.create({
    modalContainer: {
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        position: 'absolute',
        marginLeft: 30,
        height: '100%',
        width: '100%',

    },
    container: {
        alignItems: 'center',
        borderRadius: 30,
    },
    item: {
        marginBottom: 5,
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    leftItem: {
        flexDirection: 'row'
    },
    cardNum: {
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        width: 37,
        height: 37
    },
    BarCodeBox: {
        backgroundColor: Colors.light.p_light,//'#000000aa',
        height: 365,
        width: 300,
        overflow: 'hidden',
        borderRadius: 10,
        padding: 5
    },
    boton: {
        borderRadius: 16,
        width: 100,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 8
    }
})