/**
 * Prepare instance
 *
 */
const { copyAssets } = require('./prepare-instance-assets')
const { setApp } = require('./prepare-instance-app')
const instances = require('./instances')
const instanceRequired = instances.find(i => i.slug.toLocaleLowerCase() === process.env.INSTANCE_NAME.toLocaleLowerCase())

if (!instanceRequired) {
  throw new Error(`Instancia \`${process.env.INSTANCE_NAME}\` no encontrada`)
  exit(1)
}

copyAssets(instanceRequired.name)
setApp(instanceRequired)