import * as React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../../types';
import HomeScreen from '../../screens/ListPackageHome/PackageListHome'
import Details from '../../screens/DetailPackage/DetailsPackage';
import Abstract from '../../screens/Abstract/Abstract';
import { ThemeProps, useThemeColor } from '../../components/Themes/Themed'

const HomeStack = createNativeStackNavigator<RootStackParamList>();

export function HomeStackScreen() {

    return (
        <HomeStack.Navigator
            screenOptions={{
                headerStyle: {
                    //shadowColor: 'yellow', // iOS
                    elevation: 0, // Android,
                    backgroundColor: useThemeColor({}, 'p_dark')
                },
                headerTintColor: 'black',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}>
            <HomeStack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    title: 'PackageList',
                    headerTintColor: 'black',
                    headerBackTitleVisible: false,
                }} />
            <HomeStack.Screen
                name="Details"
                component={Details}
                options={{
                    title: 'Details',
                    headerBackTitleVisible: true,
                    headerBackTitle: 'Back',
                }} />
            <HomeStack.Screen
                name="Abstract"
                component={Abstract}
                options={{
                    headerBackTitleVisible: true,
                    headerBackTitle: 'Back',
                }}
            />
        </HomeStack.Navigator>
    );
}
