import React from 'react';
import { View, StyleSheet, Button, Modal, Alert, TouchableOpacity } from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner';
import { TextComun } from './Text';
import { useThemeColor } from '../Themes/Themed';

interface ScanProps {
    visible: boolean
    scan?({ }): void
    onClose?(): void
}

export function Scanner({ visible, onClose, scan }: ScanProps): any {

    const [hasPermission, setHasPermission] = React.useState<any>(null);
    const [scanned, setScanned] = React.useState(false);
    const [text, setText] = React.useState('')

    React.useEffect(() => {
        if (visible) {
            askPermission()
        }
    }, [visible])

    const askPermission = async () => {
        const { status } = await BarCodeScanner.requestPermissionsAsync();
        setHasPermission(status == 'granted')
    }

    if (hasPermission === false) {
        return Alert.alert('No tienes permisos de camara')
    }

    return (
        <View style={style.modalContainer}>
            <Modal
                transparent={true}
                animationType='fade'
                visible={visible}
                onRequestClose={onClose}>
                <View style={style.modalContainer}>
                    <View style={style.container}>
                        <View style={style.BarCodeBox}>
                            <BarCodeScanner
                                onBarCodeScanned={scan}
                                style={{ height: 400, width: 400 }}
                            />
                        </View>
                        <TouchableOpacity style={[style.boton, { backgroundColor: useThemeColor({}, 'p_dark') }]} onPress={onClose}>
                            <TextComun style={{ fontWeight: 'bold', color: useThemeColor({}, 'p_black') }} label='Close' />
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    )
}


const style = StyleSheet.create({
    modalContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        height: '100%', width: '100%',
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30
    },
    BarCodeBox: {
        backgroundColor: '#000000aa',
        alignItems: 'center',
        justifyContent: 'center',
        height: 365,
        width: 300,
        overflow: 'hidden',
        borderRadius: 30,
        padding: 5
    },
    boton: {
        borderRadius: 16,
        width: 100,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    }
})