import React from "react";
import { TextInput, Dimensions, StyleSheet } from 'react-native'
import { useThemeColor } from "../Themes/Themed";

export function SearchBar(props: TextInput['props'] & any) {
    const { style, placeholder, keyboard } = props;
    const [value, setValue] = React.useState('')

    function TamWidth() {

        if (Dimensions.get('window').width <= 360) return '60%'
        else if (Dimensions.get('window').width <= 700) return '70%'
        else return '83%'

        return '50%'
    }

    function setText(value: any) {
        setValue(value)
    }

    return (
        <TextInput
            style={[{ width: TamWidth(), backgroundColor: useThemeColor({}, 'backColor') }, styles.input, style]}
            returnKeyType='search'
            keyboardType={keyboard}
            keyboardAppearance={'dark'}
            value={value}
            autoCorrect={false}
            inlineImagePadding={2}
            underlineColorAndroid='transparent'
            onChangeText={setText}
            onSubmitEditing={() => props.onSearch &&
                props.onSearch(value)
            }
            placeholder={placeholder || 'Search'}
            selectionColor={useThemeColor({}, 'purple')}
        />
    );
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 37,
        fontFamily: 'bebas-regular',
        height: 30,
        paddingLeft: 8
    }
})