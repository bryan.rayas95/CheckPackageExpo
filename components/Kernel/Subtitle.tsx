import React from "react";
import { Text, StyleSheet } from 'react-native'
import Layout from '../../constants/Layout'

export function Subtitle(props: Text['props'] & any) {
    return (
        <Text style={[style.subtitle, props.style]}>{props.label}</Text>
    )
}

const style = StyleSheet.create({
    subtitle: {
        fontSize: Layout.isSmallDevice ? 11 : 13,
        fontFamily: 'roboto_medium'
    }
})

