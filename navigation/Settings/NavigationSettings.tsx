
import * as React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../types';
import Abstract from '../../screens/Abstract/Abstract';
import Details from '../../screens/DetailPackage/DetailsPackage';
import { useThemeColor } from '../../components/Themes/Themed';

const SettingsStack = createNativeStackNavigator<RootStackParamList>();

export function SettingsStackScreen() {
    return (
        <SettingsStack.Navigator
            screenOptions={{
                headerStyle: {
                    //shadowColor: 'yellow', // iOS
                    elevation: 0, // Android,
                    backgroundColor: useThemeColor({}, 'p_dark')
                },
                headerTintColor: 'black',
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            }}
        >
            <SettingsStack.Screen name="Abstract" component={Abstract} />
            <SettingsStack.Screen name="Details"
                component={Details}
                options={{
                    title: 'Details',
                    headerBackTitleVisible: true,
                    headerBackTitle: 'Back'
                }}
            />
        </SettingsStack.Navigator>
    );
}