import React from 'react'
import {
    StyleSheet, View, ImageBackground,
    TouchableOpacity, KeyboardAvoidingView, TextInput,
    TouchableWithoutFeedback, Keyboard, Platform, StatusBar, Alert
} from 'react-native'
import { SearchBar, Subtitle } from '../../components/Kernel/IndexComponents'
import Colors from '../../constants/Colors'
import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'
import { RetriveOneContract } from '../../hooks/useControlHome'
import { Api, Exception } from '../../components/Connetions/index'

export default function Login({ navigation }) {

    const [codigo, setCodigo] = React.useState<string>('')

    async function ValidateContract(code: string) {
        try {

            Api.setLocalSessionToken(null)

            if (code.length == 0) {
                return alert('Debes ingresar un codigo de proyecto')
            }
            let validate = await RetriveOneContract(code)

            if (validate) {
                await Api.setLocalSessionToken(validate.iId?.toString())
                Api.setSessionToken(validate.iId,'contract')
                navigation.navigate('Tabs')
            } else {
                setCodigo('')
                alert('El numero de contrato no  es valido')
            }

        } catch (error) {
            Exception.load(error).show()
        }
    }

    return (
        <ImageBackground
            source={require('../../assets/images/steel-structure-4.jpg')}
            style={styles.headerImage}
            resizeMode="cover"
        >
            <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null} style={styles.headerImage}>
                <StatusBar barStyle='light-content' />
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.div}>
                        <View style={styles.viewContainer}>
                            <View style={styles.hashtag}>
                                <FontAwesome
                                    name={'hashtag'}
                                    size={20}
                                    style={{ color: Colors.light.s_light }} />
                            </View>
                            <TextInput
                                defaultValue={codigo}
                                style={[styles.input, styles.left]}
                                placeholder={'NUMERO CONTRATO'}
                                onChangeText={(text: string) => setCodigo(text)}
                                keyboardAppearance={'dark'}
                                inlineImagePadding={2}
                                underlineColorAndroid='transparent'
                                selectionColor={Colors.light.purple}
                            />
                        </View>

                        <TouchableOpacity
                            onPress={() => ValidateContract(codigo)} style={styles.viewContainer}>
                            <View style={styles.hashtag}>
                                <MaterialCommunityIcons
                                    name={'hand-pointing-right'}
                                    size={25}
                                    style={{ color: Colors.light.s_light }} />
                            </View>
                            <View style={[styles.boton, styles.left]}>
                                <Subtitle label='ENTRAR' />
                            </View>
                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    div: {
        width: "100%",
        height: "10%",
        marginBottom: 60,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    headerImage: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    hashtag: {
        backgroundColor: Colors.light.p_light,
        height: 40,
        width: 40,
        borderLeftColor: 'gray',
        borderLeftWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        borderTopColor: 'gray',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewContainer: {
        flexDirection: 'row',
        width: "80%",
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        marginTop: 5
    },
    boton: {
        width: "80%",
        height: 40,
        justifyContent: 'center',
        alignItems: 'flex-start',
        borderRadius: 9,
        backgroundColor: Colors.light.p_light,
        borderWidth: 1,
        borderColor: 'gray',
        paddingLeft: "32%",
        //backgroundColor:'red'
    },
    input: {
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 37,
        backgroundColor: Colors.light.backColor,
        fontFamily: 'bebas-regular',
        paddingLeft: 8,
        width: "80%",
        height: 40,
    },
    left: {
        borderRadius: 10,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderLeftWidth: 0
    },
    search: {
        borderRadius: 10,
        height: 40
    }
})