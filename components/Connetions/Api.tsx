/**
 * Api
 */
import axios from 'axios';
import Status from 'http-status';
import AsyncStorage from '@react-native-async-storage/async-storage';

/** Variables Globales */
const sesionTokenName = 'package_proyecto'
var sessionTokenValue: any = {
    contract: '0',
    npackage: null
}

export default class Api {
    static urlDeveloment = 'http://192.168.1.187:4003';
    static urlProduction = 'http://187.217.102.58:4018'
    static pageSize = 35;

    /**
     * To Url
     *
     * @param {String} path
     */
    static to(path: string, query = {}) {
        const url = process.env.NODE_ENV === 'development'
            ? this.urlDeveloment + path
            : this.urlProduction + path

        if (process.env.NODE_ENV === 'development') {
            console.info('[API.TO]', url, JSON.stringify(query, undefined, 2));
        }

        return url;
    }

    /**
     * GET
     *
     * @param string path
     * @param Object params
     */
    static get(path: string, params = {}, config = {}) {
        return axios({
            method: 'GET',
            // headers: this.headers(extraHeaders),
            url: this.to(path, params),
            params,
            ...config
        });
    }

    /**
     * GET
     *
     * @param string path
     * @param Object params
     */
    static query(query = {}, extraHeaders = {}, config = {}) {
        return axios({
            method: 'GET',
            //headers: this.headers(extraHeaders),
            url: this.to('/kernel/query', query),
            params: {
                query,
            },
            ...config
        });
    }

    /**
     * POST
     *
     * @param string path
     * @param Object data
     * @param Object config
     */
    static post(path: string, data: {}, config = {}) {
        return axios({
            method: 'POST',
            //headers: this.headers(data),
            url: this.to(path, data),
            data,
            ...config
        });
    }

    /**
     * PUT
     *
     * @param string path
     * @param Object data
     * @param Object config
     */
    static put(path: string, data = {}, config = {}) {
        return axios({
            method: 'PUT',
            url: this.to(path),
            data,
            ...config
        });
    }

    /**
     * DELETE
     *
     * @param string path
     * @param Object data
     */
    static delete(path: string, config = {}) {
        return axios({
            method: 'DELETE',
            //headers: this.headers(),
            url: this.to(path),
            ...config
        });
    }

    /**
   * sessionToken
   *
   * @returns {string}
   */
    static sessionToken(): string {
       
        return sesionTokenName
    }

    /**
     * sessionToken
     */
    static localSessionToken() {
        return AsyncStorage.getItem(sesionTokenName)
    }

    /**
     * sessionToken
     */
    static setLocalSessionToken(token: string | null) {
        if (token) {
            return AsyncStorage.setItem(sesionTokenName, token)
        } else {
            return AsyncStorage.removeItem(sesionTokenName)
        }
    }

    /**
     * set sessionToken
     */
    static setSessionToken(token: string | number, name: any) {
        sessionTokenValue[name] = token.toString()
    }

    /**
     * get sessionToken
     */
     static getSessionToken( name: string) {
         return sessionTokenValue[name]
    }

    /**
     * Procesa un código de error a texto
     *
     * @param {Int} code
     */
    static statusCodeToString(code: any, message: any) {

        switch (code) {
            case Status.CONFLICT:
                return 'Conflicto de información';

            case Status.UNAUTHORIZED:
                return 'No autorizado';

            case Status.NOT_FOUND:
                return 'No encontrado';

            case Status.BAD_REQUEST:
                return message;

            default:
                return code >= Status.INTERNAL_SERVER_ERROR ? 'Servicio no disponible' : 'Error desconocido';
        }
    }
}

