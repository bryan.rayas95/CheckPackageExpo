import React from "react";
import { Text, StyleSheet } from 'react-native'
import Layout from '../../constants/Layout'
export function TextComun(props: Text['props'] & any) {
    return (
        <Text style={[style.text, props.style]}>{props.label}</Text>
    )
}

const style = StyleSheet.create({
    text: {
        fontSize: Layout.isSmallDevice ? 11 : 13,
        fontFamily: 'roboto_light'
    }
})