import { Api, Exception } from '../components/Connetions/index'

/**
 *  Return Package global report
 * @param idPackage number
 * @returns 
 */
export async function PacksGlobal(idPackage: number) {
    try {
        let response: any = []

        response = await Api.query({
            model: 'ConsPackageList',
            attributes: [
                ['iId', 'key'],
                'iIdPieza',
                'dDimLength',
                'dDimWide',
                'dDimHeight',
                'dGV',
                ['sPacking', 'name'],
                [{ key: 'fn', name: 'sum', params: ['$pieza->ensamble.dPeso$'] }, 'peso'],
                [{ fn: 'count', params: ['iId'] }, 'cant']
            ],
            include: [
                {
                    as: 'pieza',
                    requiered: true,
                    attributes: ['iId'],
                    include: [
                        {
                            as: 'ensamble',
                            required: true,
                            attributes: ['iId'],
                            include: [
                                {
                                    required: true,
                                    as: 'area',
                                    attributes: ['sCodigo']
                                }
                            ]
                        }
                    ]
                },
                {
                    as: 'package',
                    attributes: ['sMainDescription', 'sPackingStyle']
                }

            ],
            where: {
                iActivo: true,
                iIdPackage: idPackage,
            },
            group: ['sPacking']
        })

        if (response) return response.data

        return []
    } catch (error) {
        Exception.load(error).show()
    }
}

/**
 *  Return abstract Package x area report
 * @param idPackage number
 * @returns 
 */
export async function AreaGlobal(idPackage: number) {
    try {
        let response: any = []

        response = await Api.query({
            model: 'ConsPackageList',
            attributes: [
                ['iId', 'key'],
                ['iIdPieza','title'],
                [{ fn: 'sum', params: ['$pieza->ensamble.dPeso$'] }, 'peso']
            ],
            include: [
                {
                    as: 'pieza',
                    attributes: ['iId', 'iIdEnsamble'],
                    include: [
                        {
                            as: 'ensamble',
                            attributes: ['iId', 'iIdArea'],
                            include: [
                                {
                                    as: 'area',
                                    attributes: ['sCodigo']
                                }
                            ]
                        }
                    ],
                },
                {
                    as: 'package',
                    attributes: ['sTraspaso']
                }
            ],
            group: ['$pieza->ensamble.iIdArea$'],
            where: {
                iActivo: true,
                iIdPackage: idPackage,
            },

        })

        if (response) return response.data

        return []
    } catch (error) {
        Exception.load(error).show()
    }
}