/**
 * Card Traffic lights for items package
 * @author Brian Armando
 */
import React from "react";
import { View, StyleSheet } from 'react-native'
import { Title } from "./Title";
import { Separator } from "./Separator";
import { Subtitle } from "./Subtitle";

export function CardTrafficLights(props: any) {

    const DrawingColor = (val: boolean) => {
        if (val == true) return '#33A07D'
        return 'orange'
    }

    return (
        <View>
            <View style={[style.label]}>
                <View>
                    <Title style={props.title} label={props.lblName} />
                    <Subtitle label={props.pack} />
                </View>
                <View style={[style.traffic, { backgroundColor: DrawingColor(props.value) }]} />
            </View>
            <Separator />
        </View>
    )
}

const style = StyleSheet.create({
    label: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 12
    },
    traffic: {
        width: 15,
        height: 15,
        borderRadius: 17,
        marginRight: 20,
    }

})